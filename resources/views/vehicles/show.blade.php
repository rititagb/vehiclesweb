<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 80px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            table {
                border-collapse: collapse;
            }

            table, th, td {
                border: 1px solid black;
                padding: 8px;
            }
            th, td {
                text-align: left;
                padding-right: 52px;
                padding-left: 15px;
            }
        </style>
    </head>
    <body>
        <div class="">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content container">
                <div class="row title m-b-md">
                    Detail information
                </div>
                <div style="text-align: left; margin-bottom: 15px;">
                    <a href="/vehicles">< Back </a>
                </div>
                <div class="row">
                    <div style="overflow-x:auto;">
                        <table >
                            <tr>
                                <th>Manufacturer</th>
                                <td>{{ $vehicle->manufacturer }}</td>
                            </tr>
                            <tr>
                                <th>Model</th>
                                <td>{{ $vehicle->model }}</td>
                            </tr>
                            <tr>
                                <th>Type</th>
                                <td>{{ $vehicle->type }}</td>
                            </tr>
                            <tr>
                                <th>Usage</th>
                                <td>{{ $vehicle->usage }}</td>
                            </tr>
                            <tr>
                                <th>License plate</th>
                                <td>{{ $vehicle->license_plate }}</td>
                            </tr>
                            <tr>
                                <th>Weight category</th>
                                <td>{{ $vehicle->weight_category }}</td>
                            </tr>
                            <tr>
                                <th>Number of seats</th>
                                <td>{{ $vehicle->no_seats }}</td>
                            </tr>
                            <tr>
                                <th>Has boot </th>
                                <td>{{ $vehicle->has_boot ? 'True' : 'False' }}</td>
                            </tr>
                            <tr>
                                <th>Has trailer </th>
                                <td>{{ $vehicle->has_trailer ? 'True' : 'False' }}</td>
                            </tr>
                            <tr>
                                <th>Owner name </th>
                                <td>{{ $vehicle->owner_name }}</td>
                            </tr>
                            <tr>
                                <th>Owner company </th>
                                <td>{{ $vehicle->owner_company }}</td>
                            </tr>
                            <tr>
                                <th>Owner profession </th>
                                <td>{{ $vehicle->owner_profession }}</td>
                            </tr>
                            <tr>
                                <th>Transmission </th>
                                <td>{{ $vehicle->transmission }}</td>
                            </tr>
                            <tr>
                                <th>Colour </th>
                                <td>{{ $vehicle->colour }}</td>
                            </tr>
                            <tr>
                                <th>Heavy goods vehicle </th>
                                <td>{{ $vehicle->is_hgv ? 'True' : 'False' }}</td>
                            </tr>
                            <tr>
                                <th>Number of doors </th>
                                <td>{{ $vehicle->no_doors }}</td>
                            </tr>
                            <tr>
                                <th>Has sunroof </th>
                                <td>{{ $vehicle->sunroof ? 'True' : 'False' }}</td>
                            </tr>
                            <tr>
                                <th>Has gps </th>
                                <td>{{ $vehicle->has_gps ? 'True' : 'False' }}</td>
                            </tr>
                            <tr>
                                <th>Number of wheels </th>
                                <td>{{ $vehicle->no_wheels }}</td>
                            </tr>
                            <tr>
                                <th>Engine cc </th>
                                <td>{{ $vehicle->engine_cc }}</td>
                            </tr>
                            <tr>
                                <th>Fuel type </th>
                                <td>{{ $vehicle->fuel_type }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
