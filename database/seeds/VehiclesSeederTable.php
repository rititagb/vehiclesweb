<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehiclesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $xmlfile = file_get_contents(database_path("data/VehicleSample.xml"));
        $xml= simplexml_load_string($xmlfile);

        foreach ($xml as $obj) {
            DB::table('vehicles')->insert([
                'manufacturer' => $obj->attributes()->manufacturer,
                'model' => $obj->attributes()->model,
                'type' => $obj->type,
                'usage' => $obj->usage,
                'license_plate' => $obj->license_plate,
                'weight_category' => $obj->weight_category,
                'no_seats' => $obj->no_seats,
                'has_boot' => (boolean) $obj->has_boot,
                'has_trailer' => (boolean) $obj->has_trailer,
                'owner_name' => $obj->owner_name,
                'owner_company' => $obj->owner_company,
                'owner_profession' => $obj->owner_profession,
                'transmission' => $obj->transmission,
                'colour' => $obj->colour,
                'is_hgv' => (boolean) $obj->is_hgv,
                'no_doors' => $obj->no_doors,
                'sunroof' => (boolean) $obj->sunroof,
                'has_gps' => (boolean) $obj->has_gps,
                'no_wheels' => $obj->no_wheels,
                'engine_cc' => $obj->engine_cc,
                'fuel_type' => $obj->fuel_type
            ]);
        }
    }
}
