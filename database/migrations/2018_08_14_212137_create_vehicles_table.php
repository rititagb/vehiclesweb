<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('manufacturer');
            $table->string('model');
            $table->string('type');
            $table->string('usage');
            $table->string('license_plate');
            $table->string('weight_category');
            $table->integer('no_seats');
            $table->boolean('has_boot');
            $table->boolean('has_trailer');
            $table->string('owner_name');
            $table->string('owner_company');
            $table->string('owner_profession');
            $table->string('transmission');
            $table->string('colour');
            $table->boolean('is_hgv');
            $table->integer('no_doors');
            $table->boolean('sunroof');
            $table->boolean('has_gps');
            $table->integer('no_wheels');
            $table->integer('engine_cc');
            $table->string('fuel_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
