<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vehicles';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
        'has_boot' => 'boolean',
        'has_trailer' => 'boolean',
        'is_hgv' => 'boolean',
        'sunroof' => 'boolean',
        'has_gps' => 'boolean'
    ];
}
