<?php

namespace App\Http\Controllers;

use App\Vehicle;

class VehiclesController extends Controller
{
    //
    public function getVehicles () {
        return Vehicle::all();
    }

    public function getVehicle ($id) {
        return Vehicle::find($id);
    }
    public function index () {
        $vehicles = $this->getVehicles();
        return view('vehicles.index', compact('vehicles'));
    }

    /* Returns a JSON payload */
    public function index_api()
    {
        return response()->json($this->getVehicles());
    }

    public function show ($id) {
        $vehicle = $this->getVehicle($id);
        return view('vehicles.show', compact('vehicle'));
    }

    /* Returns a JSON payload */
    public function show_api($id)
    {
        return response()->json($this->getVehicle($id));
    }

}
